/***************************************************************************
 *   Copyright (C) 2012 by Daniel Nicoletti <dantti12@gmail.com>           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; see the file COPYING. If not, write to       *
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,  *
 *   Boston, MA 02110-1301, USA.                                           *
 ***************************************************************************/

#include "Description.h"
#include "ui_Description.h"

#include <QStringBuilder>
#include <QtDBus/QDBusInterface>
#include <QtDBus/QDBusReply>

#include <KGlobal>
#include <KLocale>
#include <KDateTime>
#include <KToolInvocation>
#include <KDebug>
#include <KMessageWidget>

#include <QtNetworkManager/manager.h>
#include <QtNetworkManager/ipv4config.h>

typedef QList<QDBusObjectPath> ObjectPathList;
typedef QMap<QString, QString>  StringStringMap;

Description::Description(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Description)
{
    ui->setupUi(this);

    // Message Widget setup
    ui->msgWidget->setMessageType(KMessageWidget::Information);
    ui->msgWidget->setWordWrap(true);
    ui->msgWidget->setCloseButtonVisible(false);
    ui->msgWidget->hide();
}

Description::~Description()
{
    delete ui;
}

int Description::innerHeight() const
{
    return ui->tabWidget->currentWidget()->height();
}

void Description::setDevice(const QString &uni)
{
    while (ui->tabWidget->count() > 1) {
        ui->tabWidget->removeTab(1);
    }

    ui->stackedWidget->setCurrentIndex(1);

    NetworkManager::Device *device = NetworkManager::findNetworkInterface(uni);
    ui->deviceIdL->setText(device->interfaceName());
    QString addressText;
    foreach (const QNetworkAddressEntry &address, device->ipV4Config().addresses()) {
        addressText.append(address.ip().toString());
    }
    kDebug() << device->ipV4Config().routes().size();

    ui->addressL->setText(addressText);
}

#include "Description.moc"
